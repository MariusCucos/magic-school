import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {HousesComponent} from './houses/houses.component';
import {HouseComponent} from './house/house.component';
import {CharacterComponent} from './character/character.component';
import {CharactersComponent} from './characters/characters.component';
import {LoginComponent} from "./login/login.component";
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'houses', component: HousesComponent, canActivate: [AuthGuard]  },
  { path: 'houses/:id', component: HouseComponent, canActivate: [AuthGuard]  },
  { path: 'characters', component: CharactersComponent, canActivate: [AuthGuard] },
  { path: 'characters/:id', component: CharacterComponent, canActivate: [AuthGuard]  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
