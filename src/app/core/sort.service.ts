import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class SortService {

  constructor() { }

  property: string = null;
  direction = 1;

  sort(collection: any[], prop: any) {
    this.property = prop;
    this.direction =  this.direction * -1;

    collection.sort((a: any, b: any) => {
      let aVal: any;
      let bVal: any;

      aVal = a[prop];
      bVal = b[prop];

      if (aVal === bVal) {
        return 0;
      } else if (aVal > bVal){
        return this.direction * -1;
      } else {
        return this.direction;
      }
    });
  }

}
