import { Component, OnInit } from '@angular/core';
import { DataService } from '../core/data.service';
import {IHouse} from '../shared/Interfaces';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.scss']
})
export class HousesComponent implements OnInit {

  houses: IHouse[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getHouses().subscribe((houses: IHouse[]) => {
      this.houses = houses;
    });
  }

}
