import { Component, OnInit } from '@angular/core';
import { DataService } from "../core/data.service";
import {Router} from "@angular/router";
import {IUser} from "../shared/Interfaces";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  users: IUser[] = [];
  email: '';
  password: '';
  isLoggedIn: boolean;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.dataService.getUsers().subscribe(users => {
      this.users = users;
    });
    this.isLoggedIn = this.checkIfIsLoggedIn();
  }

  login(form) {
    this.users.map(user => {
      if (user.email === form.value.email && user.password === form.value.password ) {
        sessionStorage.setItem('token', user.token);
        this.router.navigate( [''] );
      }
    });
  }

  checkIfIsLoggedIn() {
    if (sessionStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    sessionStorage.clear();
    location.reload();
  }
}
