import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';
import { DataService } from '../core/data.service';
import { IMember } from '../shared/Interfaces';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  character: IMember[] = [];

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.dataService.getCharacterById(id).subscribe(character => {
      this.character = character;
    });
  }

}
