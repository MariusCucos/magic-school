import { Component, OnInit } from '@angular/core';
import { IMember } from '../shared/Interfaces';
import { DataService } from '../core/data.service';
import { SortService } from '../core/sort.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  characters: IMember[] = [];

  constructor(private dataService: DataService, private sortService: SortService) { }

  ngOnInit() {
    this.dataService.getCharacters().subscribe((characters: IMember[]) => {
      this.characters = characters;
    });
  }

  sort(prop: string) {
    this.sortService.sort(this.characters, prop);
  }

}
