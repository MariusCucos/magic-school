export const environment = {
  production: true,
  apiKey : '$2a$10$d2QcWUCBLJ07w6IXbzw/e.geehpSLI7bBntISR3rvBKrWfiKyoHxi',
  apiHost : 'https://www.potterapi.com',
  apiVersion : 'v1',
};
