import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../core/data.service';
import { SortService } from '../core/sort.service';
import { IHouse, IMember } from '../shared/Interfaces';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.scss']
})
export class HouseComponent implements OnInit {

  house: IHouse[] = [];
  houseMembers: IMember[] = [];
  members: any;

  constructor(private dataService: DataService, private route: ActivatedRoute, private sortService: SortService) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.dataService.getHouseById(id).subscribe(house => {
      this.house = house[0];
      this.members = house[0].members;
    });

    this.dataService.getCharacters().subscribe(characters => {
      const filteredChars = characters.filter(character => {
        return this.members.find(houseMember => houseMember._id === character._id);
      });
      this.houseMembers = filteredChars.map(member => {
        const [firstName, ...rest] = member.name.split(' ');
        const lastName = rest.pop();
        member.firstName = firstName;
        member.lastName = lastName;
        return member;
      });
    });
  }

  sort(prop: string) {
    this.sortService.sort(this.houseMembers, prop);
  }

}
