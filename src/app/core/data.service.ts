import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getHouses(): Observable<any> {
    return this.http.get(`${environment.apiHost}/${environment.apiVersion}/houses/?key=${environment.apiKey}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  getHouseById(id: string): Observable<any> {
    return this.http.get(`${environment.apiHost}/${environment.apiVersion}/houses/${id}/?key=${environment.apiKey}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  getCharacters(): Observable<any> {
    return this.http.get(`${environment.apiHost}/${environment.apiVersion}/characters/?key=${environment.apiKey}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  getCharacterById(id: string): Observable<any> {
    return this.http.get(`${environment.apiHost}/${environment.apiVersion}/characters/${id}/?key=${environment.apiKey}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  getUsers(): Observable<any> {
    return this.http.get('../assets/users.json')
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: any) {
    console.error('server error:', error);
    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return Observable.throw(errMessage);
    }
    return Observable.throw(error || 'Server error');
  }
}
