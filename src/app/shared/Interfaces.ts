export interface IHouse {
  _id: string;
  colors: [];
  headOfHouse: string;
  houseGhost: string;
  mascot: string;
  members: [];
  name: string;
  school: string;
  values: [];
}

export interface IMember {
  _id: string;
  name: string;
  house: string;
  patronus: string;
  species?: string;
  firstName?: string;
  lastName?: string;
  bloodStatus: string;
  role: string;
  school: string;
  deathEater: boolean;
  dumbledoresArmy: boolean;
  orderOfThePhoenix: boolean;
  ministryOfMagic: boolean;
  alias: string;
  wand: string;
  boggart: string;
  animagus: string;
}

export interface IUser {
  email: string;
  password: string;
  token?: string;
}
